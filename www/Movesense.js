const exec = require('cordova/exec');

exports.prepare = function(success, error) {
    exec(success, error, "MovesensePlugin", "prepare", []);
};

exports.scan = function(success, error) {
    exec(success, error, "MovesensePlugin", "scan", []);
};

exports.stopScan = function(success, error) {
    exec(success, error, "MovesensePlugin", "stopScan", []);
};

exports.connect = function(name, success, error) {
    exec(success, error, "MovesensePlugin", "connect", [name]);
};

exports.subscribe = function(uri, contract, subscriptionKey, success, error) {
    exec(success, error, "MovesensePlugin", "subscribe", [uri, contract, subscriptionKey]);
};

exports.unsubscribe = function(key, success, error) {
    exec(success, error, "MovesensePlugin", "unsubscribe", [key]);
};

exports.get = function(uri, contract, success, error) {
    request('get', uri, contract, success, error);
};

exports.post = function(uri, contract, success, error) {
    request('post', uri, contract, success, error);
};

exports.put = function(uri, contract, success, error) {
    request('put', uri, contract, success, error);
};

exports.del = function(uri, contract, success, error) {
    request('del', uri, contract, success, error);
};

function request(method, uri, contract, success, error) {
    exec(success, error, "MovesensePlugin", method, [uri, contract]);
};
