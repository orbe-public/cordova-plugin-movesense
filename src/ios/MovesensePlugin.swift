import Foundation
import Bluejay
import Movesense

// Declaring this here prevents thread concurrency issues.
var devices = Dictionary<String, MovesenseDevice>()
let mds = MDSWrapper()
let bluejay = Bluejay()
let jsonDecoder: JSONDecoder = JSONDecoder()
var cdvConnnectionCallbackId: String = ""
var subscriptions = Dictionary<String, String>()
//var connected = false
var deviceUUID = UUID()

@objc(MovesensePlugin) class MovesensePlugin: CDVPlugin {
    @objc(prepare:)
    func prepare(command: CDVInvokedUrlCommand) {
        let connectionUri = "MDS/ConnectedDevices"
        bluejay.start()
    
        // Subscribes to device connection events.
        mds.doSubscribe(connectionUri,
            contract: [:],
            response: {(response) in
                let pluginResult: CDVPluginResult
            
                if (response.statusCode > 300) {
                    let message = "Subscription to connected devices failed with status code \(response.statusCode)."
                    pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: message)
                } else {
                    pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
                }

                self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
            },
            onEvent: { [weak self] (event) in
                guard let self = self else { return }
            
                guard let decodedEvent = try? jsonDecoder.decode(
                    MovesenseDeviceEvent.self, from: event.bodyData
                ) else {
                    return
                }
            
                let pluginResult: CDVPluginResult
            
                switch decodedEvent.eventMethod {
                case .post:
                    let serial = decodedEvent.eventBody.serialNumber
                    pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: serial)
                    //mds.doUnsubscribe(connectionUri)
                case .del:
                    mds.disconnectPeripheral(with: deviceUUID)
                    // TODO: Handle disconnections here (+ remove doUnsubscribe from above).
                    pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
                default:
                    pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
                }
                self.commandDelegate.send(pluginResult, callbackId: cdvConnnectionCallbackId)
            }
        )
    }

    @objc(scan:)
    func scan(command: CDVInvokedUrlCommand) {
        let movesenseServiceUUID = ServiceIdentifier(uuid: "FDF3")
        
        bluejay.scan(
            serviceIdentifiers: [movesenseServiceUUID],
            discovery: { [weak self] _, discoveries -> ScanAction in
                guard let self = self else {
                    return .stop
                }

                let scanResults = discoveries.map { ScanDiscovery -> String in
                    let name = ScanDiscovery.peripheralIdentifier.name
                    let uuid = ScanDiscovery.peripheralIdentifier.uuid
                    let serial = name.replacingOccurrences(of: "Movesense ", with: "")
                    
                    // Adds each new scan result to known devices.
                    devices[serial] = MovesenseDevice(uuid: uuid, name: name, serial: serial)
                    
                    return name
                }

                if (!scanResults.isEmpty) {
                    let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: scanResults)
                    pluginResult?.setKeepCallbackAs(true)
                    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
                }

                return .continue
            },
            stopped: { (discoveries, error) in
                if let error = error {
                    debugPrint("Scan stopped with error: \(error.localizedDescription)")
                } else {
                    debugPrint("Scan stopped without error.")
                }
            }
        )
    }
    
    @objc(stopScan:)
    func stopScan(command: CDVInvokedUrlCommand) {
        if (bluejay.isScanning) {
            bluejay.stopScanning()
        }
    }

    @objc(connect:)
    func connect(command: CDVInvokedUrlCommand) {
        let pluginResult: CDVPluginResult
        
        guard let deviceName = command.arguments[0] as? String else {
            let message = "Missing device name."
            pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: message)
            commandDelegate.send(pluginResult, callbackId: command.callbackId)
            return
        }

        let serial = deviceName.replacingOccurrences(of: "Movesense ", with: "")
        
        guard let device = devices[serial] else {
            let message = "The device \(deviceName) was not discovered. Please (re)run a scan before trying to connnect."
            pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: message)
            commandDelegate.send(pluginResult, callbackId: command.callbackId)
            return
        }

        // Sets the global callbackId for connection event handlers.
        cdvConnnectionCallbackId = command.callbackId

        deviceUUID = device.uuid
        
        //if (connected) {
            /*let disconnected = mds.disconnectPeripheral(with: deviceUUID)
            if (!disconnected) {
                let message = "The device \(deviceName) could not been disconnected."
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: message)
                commandDelegate.send(pluginResult, callbackId: command.callbackId)
                return
            }
            connected = false;
            print("DISCONNECTED : ", device.uuid, separator: " —— ")*/
        //}
        //if (!connected) {
            mds.connectPeripheral(with: deviceUUID)
            //connected = true
        //}
        
    }

    @objc(subscribe:)
    func subscribe(command: CDVInvokedUrlCommand) {
        let uri: String = command.arguments[0] as! String
        let contract: Dictionary = command.arguments[1] as! Dictionary<String, Any>
        let key: String = command.arguments[2] as! String
        
        mds.doSubscribe(uri,
            contract: contract,
            response: {(response) in
                if (response.statusCode > 300) {
                    let message = "Subscription to \(uri) failed with status code \(response.statusCode)."
                    let pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: message)
                    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
                }
    
                subscriptions[key] = uri
            },
            onEvent: { [weak self] (event) in
                guard let self = self else { return }
            
                let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: event.bodyDictionary)
                pluginResult?.setKeepCallbackAs(true)

                self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
            }
        )
    }
    
    @objc(unsubscribe:)
    func unsubscribe(command: CDVInvokedUrlCommand) {
        let key: String = command.arguments[0] as! String

        if (subscriptions[key] == nil) {
            return
        }
        
        mds.doUnsubscribe(subscriptions[key]!)
        subscriptions.removeValue(forKey: key)
    }
    
    @objc(get:)
    func get(command: CDVInvokedUrlCommand) {
        let uri: String = command.arguments[0] as! String
        let contract: Dictionary = command.arguments[1] as! Dictionary<String, Any>
    
        mds.doGet(uri, contract: contract, completion: handleResponse(command: command))
    }
    
    @objc(post:)
    func post(command: CDVInvokedUrlCommand) {
        let uri: String = command.arguments[0] as! String
        let contract: Dictionary = command.arguments[1] as! Dictionary<String, Any>
    
        mds.doPost(uri, contract: contract, completion: handleResponse(command: command))
    }
    
    @objc(put:)
    func put(command: CDVInvokedUrlCommand) {
        let uri: String = command.arguments[0] as! String
        let contract: Dictionary = command.arguments[1] as! Dictionary<String, Any>
    
        mds.doPut(uri, contract: contract, completion: handleResponse(command: command))
    }
    
    @objc(del:)
    func del(command: CDVInvokedUrlCommand) {
        let uri: String = command.arguments[0] as! String
        let contract: Dictionary = command.arguments[1] as! Dictionary<String, Any>
    
        mds.doDelete(uri, contract: contract, completion: handleResponse(command: command))
    }
    
    private func handleResponse(command: CDVInvokedUrlCommand) -> MDSResponseBlock {
        return {(response) in
            let pluginResult: CDVPluginResult
            
            if (response.statusCode > 300) {
                let message = "Request to \(command.arguments[0]) failed with status code \(response.statusCode)."
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: message)
            } else {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: response.bodyDictionary)
            }
            
            self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
        }
    }
}

public struct MovesenseDevice {
    public var uuid: UUID
    public var name: String
    public var serial: String
    public var connected: Bool = false

    init(uuid: UUID, name: String, serial: String) {
        self.uuid = uuid
        self.name = name
        self.serial = serial
    }
}
