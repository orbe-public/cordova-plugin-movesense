import Foundation

public enum MovesenseMethod: String, Codable {
    case get = "GET"
    case put = "PUT"
    case del = "DEL"
    case post = "POST"
    case subscribe
    case unsubscribe
}

public enum MovesenseResponseCode: Int, Codable {
    case unknown = 0
    case ok = 200
    case created = 201
    case badRequest = 400
    case notFound = 404
    case conflict = 409
}

public struct MovesenseConnectionInfo: Codable {
    let connectionType: String
    let connectionUuid: UUID
    
    enum CodingKeys: String, CodingKey {
        case connectionType = "Type"
        case connectionUuid = "UUID"
    }
}

public struct MovesenseAddressInfo: Codable {
    public let address: String
    public let name: String
}

struct MovesenseDeviceEventBody: Codable {
    let serialNumber: String
    let connectionInfo: MovesenseConnectionInfo?
    let deviceInfo: MovesenseDeviceInfo?
    
    enum CodingKeys: String, CodingKey {
        case serialNumber = "Serial"
        case connectionInfo = "Connection"
        case deviceInfo = "DeviceInfo"
    }
}

struct MovesenseDeviceEventStatus: Codable {
    let status: MovesenseResponseCode
    
    enum CodingKeys: String, CodingKey {
        case status = "Status"
    }
}

struct MovesenseDeviceEvent: Codable {
    let eventUri: String
    let eventStatus: MovesenseDeviceEventStatus
    let eventMethod: MovesenseMethod
    let eventBody: MovesenseDeviceEventBody
    
    enum CodingKeys: String, CodingKey {
        case eventStatus = "Response"
        case eventMethod = "Method"
        case eventUri = "Uri"
        case eventBody = "Body"
    }
}

public struct MovesenseDeviceInfo: Codable {
    public let description: String
    public let mode: Int
    public let name: String
    public let serialNumber: String
    public let swVersion: String
    public let hwVersion: String
    public let hwCompatibilityId: String
    public let manufacturerName: String
    public let pcbaSerial: String
    public let productName: String
    public let variantName: String
    public let addressInfo: [MovesenseAddressInfo]
    
    enum CodingKeys: String, CodingKey {
        case description = "Description"
        case mode = "Mode"
        case name = "Name"
        case serialNumber = "Serial"
        case swVersion = "SwVersion"
        case hwVersion = "hw"
        case hwCompatibilityId = "hwCompatibilityId"
        case manufacturerName = "manufacturerName"
        case pcbaSerial = "pcbaSerial"
        case productName = "productName"
        case variantName = "variant"
        case addressInfo = "addressInfo"
    }
}
