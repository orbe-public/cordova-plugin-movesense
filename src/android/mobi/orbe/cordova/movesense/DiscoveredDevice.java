package mobi.orbe.cordova.movesense;

import com.polidea.rxandroidble2.RxBleDevice;
import com.polidea.rxandroidble2.RxBleScanResult;

public class DiscoveredDevice {
    public String name;
    public String macAddress;
    public String serial;
    public int rssi;

    public DiscoveredDevice(RxBleScanResult scanResult) {
        this.name = scanResult.getBleDevice().getName();
        this.macAddress = scanResult.getBleDevice().getMacAddress();
        this.rssi = scanResult.getRssi();
    }

    public boolean isConnected() {
        return (this.serial != null);
    }

    public void markConnected(String serial) {
        this.serial = serial;
    }

    public void markDisconnected() {
        this.serial = null;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof DiscoveredDevice && ((DiscoveredDevice)object).macAddress.equals(this.macAddress)) {
            return true;
        } else if (object instanceof RxBleDevice && ((RxBleDevice)object).getMacAddress().equals(this.macAddress)) {
            return true;
        } else {
            return false;
        }
    }
}
