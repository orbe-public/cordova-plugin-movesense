package mobi.orbe.cordova.movesense;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import com.movesense.mds.Mds;
import com.movesense.mds.MdsConnectionListener;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsNotificationListener;
import com.movesense.mds.MdsResponseListener;
import com.movesense.mds.MdsSubscription;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PermissionHelper;
import org.apache.cordova.PluginResult;
import com.polidea.rxandroidble2.RxBleClient;
import com.polidea.rxandroidble2.RxBleDevice;
import io.reactivex.disposables.Disposable;
import io.reactivex.Observable;

/**
* Simple Cordova plugin for Movesense devices.
*/
public class MovesensePlugin extends CordovaPlugin {
    private Mds mMds;
    static private RxBleClient mBleClient;
    private Context context;
    private CallbackContext permissionContext;
    private Disposable mScanSubscription;
    private final ArrayList<DiscoveredDevice> discoveredDevices = new ArrayList<DiscoveredDevice>();
    private Map<String, MdsSubscription> subscriptionMap;

    // TODO: 15/06/2022 Handle Android platform-specific permissions (e.g for API 30+)
    private final static String[] PERMISSIONS = {
        Manifest.permission.BLUETOOTH,
        Manifest.permission.BLUETOOTH_ADMIN,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
    };

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);

        context = cordova.getActivity().getApplicationContext();
        mMds = Mds.builder().build(context);
        subscriptionMap = new HashMap<>();
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        String uri;
        String contract;
        String key;

        switch (action) {            
            case "prepare":
                this.prepare(callbackContext);
                return true;
            case "scan":
                this.scan(callbackContext);
                return true;
            case "stopScan":
                this.stopScan(callbackContext);
                return true;
            case "connect":
                final String name = args.getString(0);
                this.connect(name, callbackContext);
                return true;
            case "get":
                uri = args.getString(0);
                contract = args.getString(1);
                mMds.get(uri, contract, this.getResponseListener(callbackContext));
                return true;
            case "put":
                uri = args.getString(0);
                contract = args.getString(1);
                mMds.put(uri, contract, this.getResponseListener(callbackContext));
                return true;
            case "post":
                uri = args.getString(0);
                contract = args.getString(1);
                mMds.post(uri, contract, this.getResponseListener(callbackContext));
                return true;
            case "del":
                uri = args.getString(0);
                contract = args.getString(1);
                mMds.delete(uri, contract, this.getResponseListener(callbackContext));
                return true;
            case "subscribe":
                uri = args.getString(0);
                contract = args.getString(1);
                key = args.getString(2);
                this.subscribe(uri, contract, key, callbackContext);
                return true;
            case "unsubscribe":
                key = args.getString(0);
                this.unsubscribe(key);
                return true;
            default:
                return false;
        }
    }
    
    private void prepare(CallbackContext callbackContext) {
        permissionContext = callbackContext;
        getPermissions();
    }

    private void scan(CallbackContext callbackContext) {
        discoveredDevices.clear();

        mScanSubscription = getBleClient().observeStateChanges()
            .startWith(mBleClient.getState())
            .switchMap(state -> {
                switch (state) {
                    case READY:
                        return mBleClient.scanBleDevices();
                    case BLUETOOTH_NOT_AVAILABLE:
                    case LOCATION_PERMISSION_NOT_GRANTED:
                    case BLUETOOTH_NOT_ENABLED:
                    case LOCATION_SERVICES_NOT_ENABLED:
                        callbackContext.error(state);
                    default:
                        return Observable.empty();
                }
            }
        ).subscribe(
            scanResult -> {
                if (scanResult.getBleDevice() != null) {
                    final String name = scanResult.getBleDevice().getName();
                    
                    if (name != null && name.startsWith("Movesense")) {
                        final DiscoveredDevice device = new DiscoveredDevice(scanResult);

                        if (!discoveredDevices.contains(scanResult)) {
                            // Adds the device to the list if it hadn't been discovered before.
                            discoveredDevices.add(0, device);

                            final List<String> deviceNames = new ArrayList<>();

                            for (DiscoveredDevice d : discoveredDevices) {
                                String s = d.name;
                                deviceNames.add(s);
                            }

                            // Sends the updated list of device names.
                            final JSONArray result = new JSONArray(deviceNames);
                            
                            PluginResult r = new PluginResult(
                                PluginResult.Status.OK,
                                result
                            );

                            r.setKeepCallback(true);
                            callbackContext.sendPluginResult(r);
                        } else {
                            // Replaces if already discovered.
                            discoveredDevices.set(discoveredDevices.indexOf(device), device);
                        }
                    }
                }
            },
            throwable -> {
                callbackContext.error("There was an error with the scanning.");
            }
        );
    }

    private RxBleClient getBleClient() {
        if (mBleClient == null) {
            mBleClient = RxBleClient.create(context);
        }

        return mBleClient;
    }

    public boolean hasPermission() {
        for (String p : PERMISSIONS) {
            if(!PermissionHelper.hasPermission(this, p)) {
                return false;
            }
        }

        return true;
    }

    private void getPermissions() {
        if(hasPermission()) {
            permissionContext.success("Already has permission to use BLE.");
            return;
        }

        PermissionHelper.requestPermissions(this, 0, PERMISSIONS);
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) throws JSONException {
        for (int r : grantResults) {
            if (r == PackageManager.PERMISSION_DENIED) {
                permissionContext.error("The device was not granted enough permissions to use the plugin.");
                return;
            }
        }

        permissionContext.success("Bluetooth permissions granted!");
    }

    private void stopScan(CallbackContext callbackContext) {
        mScanSubscription.dispose();
    }

    private void connect(String deviceName, CallbackContext callbackContext) {
        DiscoveredDevice device = null;

        for (DiscoveredDevice d : discoveredDevices) {
            if (d.name.equals(deviceName)) {
                device = discoveredDevices.get(discoveredDevices.indexOf(d));
            }
        }

        if (device == null) {
            callbackContext.error("The device " + deviceName + " was not discovered. Please (re)run a scan before trying to connect.");
            return;
        }

        if (device.isConnected()) {
            callbackContext.error("The device " + deviceName + " is already connected.");
            return;
        }

        RxBleDevice bleDevice = getBleClient().getBleDevice(device.macAddress);

        mMds.connect(bleDevice.getMacAddress(), new MdsConnectionListener() {
            @Override
            public void onConnect(String macAddress) {}

            @Override
            public void onConnectionComplete(String macAddress, String serial) {
                for (DiscoveredDevice d : discoveredDevices) {
                    if (d.macAddress.equalsIgnoreCase(macAddress)) {
                        d.markConnected(serial);
                        callbackContext.success(serial);
                        break;
                    }
                }
            }

            @Override
            public void onError(MdsException e) {
                callbackContext.error("Error while trying to connect to " + deviceName + ": " + e.getMessage());
            }

            @Override
            public void onDisconnect(String macAddress) {
                for (DiscoveredDevice d : discoveredDevices) {
                    if (macAddress.equals(d.macAddress)) {
                        d.markDisconnected();
                    }
                }
            }
        });
    }

    private void subscribe(String uri, String contract, String key, CallbackContext callbackContext) {
        MdsSubscription subscription = mMds.subscribe(uri, contract, new MdsNotificationListener() {
            @Override
            public void onNotification(String s) {
                PluginResult r = new PluginResult(
                    PluginResult.Status.OK,
                    s
                );

                r.setKeepCallback(true);
                callbackContext.sendPluginResult(r);
            }

            @Override
            public void onError(MdsException e) {
                callbackContext.error("Error regarding subscription to " + uri + ":" + e.getMessage());
            }
        });

        subscriptionMap.put(key, subscription);
    }

    private void unsubscribe(String key) {
        MdsSubscription subscription = subscriptionMap.get(key);

        subscription.unsubscribe();
        subscriptionMap.remove(key);
    }
    
    private MdsResponseListener getResponseListener(CallbackContext callbackContext) {
        return new MdsResponseListener() {
            @Override
            public void onSuccess(String s) {
                callbackContext.success(s);
            }

            @Override
            public void onError(MdsException e) {
                callbackContext.error(e.getMessage());
            }
        };
    }
}
