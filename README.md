# cordova-plugin-movesense

**[Work In Progress]** Cordova plugin for using [Movesense](https://www.movesense.com/) devices. It implements it's [REST API](https://movesense.com/docs/esw/api_reference/) in order to cover all available features. Please refer to the official documentation for more information on these features.

## Available platforms

- Android
- iOS

## Installation

```shell
cordova plugin add https://gitlab.com/orbe-public/cordova-plugin-movesense.git 
```

## Usage

### Initialization

```javascript
// Initializes the plugin when cordova is ready.
function onDeviceReady() {
    Movesense.prepare(() => {
        // The plugin has been successfully initialized.
    }, (err) => {
        // The plugin initialization failed.
    });
}
```

### Scanning

Even if you already know the serial number you want to connect to, a scan must be done for the plugin to collect information on the device.

```javascript
Movesense.scan((scanResult) => {
    // This callback executes each time a new Movesense device is discovered.
    // scanResult is an array of Strings containing every known device names :
    // E.g: ["Movesense 284627465142", "Movesense 213647836472"]
}, (err) => {
    // There was an error with the scanning.
});
```

To stop receiving the scan results, use  `stopScan()`.

```javascript
Movesense.stopScan();
```

### Connection

A successful connection to the device is required prior to any other request.

```javascript
// The name of the device as given in the scan results.
const deviceName = 'Movesense 212336475623';

Movesense.connect(deviceName, (serialNumber) => {
    // This callback executes when the device is fully connected.
    // serialNumber is the 12 digits identifier written on the back of the device.
}, (err) => {
    // The connection to the device failed.
});
```

### Subscription

Movesense subscriptions are used for receiving realtime data from the device.

```javascript
// Please refer to Movesense API documentation for resource paths.
const path = '212336475623/Meas/IMU9/13';

// Unique identifier used to handle (add and dispose of) subscriptions.
const subKey = uuid();

let uri;
let contract = {};

// API requests to a Movesense device vary between platforms.
if (iOS) {
    uri = path;
} else {
    uri = 'suunto://MDS/EventListener';

    contract = {
        Uri: path,
    }
}

Movesense.subscribe(uri, contract, subKey, (newData) => {
    // This fires for every new data comming from the device.
}, (err) => {
    // There was an error with the subscription.
});
```

### Unsubscription

To unsubscribe from a Movesense resource, we use the subscription key that was created for the subscription.

```javascript
Movesense.unsubscribe(subKey);
```

### Requests

The Movesense plugin implements all Movesense API methods (GET, POST, PUT, DELETE), with corresponding functions: `Movesense.get()`, `Movesense.post()`, `Movesense.put()` and `Movesense.del()`. It works similarily to subscriptions without the need for a subscription key.

```javascript
Movesense.get(uri, contract, (response) => {
    // This is called on device response.
}, (err) => {
    // There was an error with the request.
});
```

## Todo

- [ ] Add device disconnection function.
- [ ] Prevent the cordova-plugin-add-swift-support (required by iOS) from installing on Android.
- [ ] Review Android BLE permissions for the latest platforms.
- [ ] Add Android and iOS version compatibility.

## Credits

This plugin was heavily inspired by the [React Native Movesense library](https://github.com/Johan-dutoit/react-native-movesense).